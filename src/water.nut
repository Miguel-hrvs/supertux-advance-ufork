::Water <- class extends Actor {
	shape = 0
	substance = "water"

	constructor(_x, _y, _arr = null) {
		base.constructor(_x, _y)
		if(_arr != null && _arr != "")
			substance = _arr
	}

	function run() {
		if(gvPlayer && hitTest(shape, gvPlayer.shape))
			handleHits(gvPlayer)

		if(gvPlayer2 && hitTest(shape, gvPlayer2.shape))
			handleHits(gvPlayer2)
	}

	function handleHits(target) {
    local statsCondition = "stats" in target && !target.invincible && !target.blinking
    local xRand = target.x + target.shape.w - randInt(target.shape.w * 2)
    local yRand = target.y + target.shape.h - randInt(target.shape.h * 2)

    switch(substance) {
        case "lava":
            target.x = (target.x + target.xprev) / 2.0
            target.y = (target.y + target.yprev) / 2.0
            if(statsCondition)
                target.stats.health -= 0.2 * target.damageMult.fire
            if(getFrames() % 8 == 0) {
                newActor(FlameTiny, xRand, yRand)
                popSound(sndFlame)
            }
            break
        case "acid":
            if(statsCondition)
                target.stats.health -= 0.1 * target.damageMult.toxic
            if(getFrames() % 8 == 0) {
                local c = actor[newActor(AcidBubble, xRand, yRand)]
                c.vspeed = -0.2
                popSound(sndBlurp)
            }
            break
        case "swamp":
        case "honey":
            if("stats" in target && target.stats.weapon == "water")
                break
            target.x = (target.x + target.xprev) / 2.0
            target.y = (target.y + target.yprev) / 2.0
            break
    }
}

	function draw() {
    local xRand, yRand, jZero;
    switch(substance) {
        case "acid":
            for(local i = 0; i < shape.w / 8; i++) {
                xRand = x - shape.w - floor(camx) + (i * 16);
                for(local j = 0; j < shape.h / 8; j++) {
                    yRand = y - shape.h - camy + (j * 16);
                    jZero = j == 0;
                    drawSprite((jZero ? sprAcidSurface : sprAcid), getFrames() / 16, xRand, yRand, 0, 0, 1, 1, 0.5)
                }
            }
            break
        case "lava":
            for(local i = 0; i < shape.w / 8; i++) {
                xRand = x - shape.w - floor(camx) + (i * 16);
                for(local j = 0; j < shape.h / 8; j++) {
                    yRand = y - shape.h - camy + (j * 16);
                    jZero = j == 0;
                    drawSprite((jZero ? sprLavaSurface : sprLava), (jZero ? (getFrames() / 16) + (i * i) : getFrames() / 16), xRand, yRand, 0, 0, 1, 1, 0.5)
                }
            }
            for(local i = 0; i < shape.w / 8; i++) {
                xRand = x - shape.w - floor(camx) + (i * 16) + 8;
                drawLight(sprLightFire, i + getFrames() / 8, xRand, y - shape.h - camy - 4, (getFrames() / 4) + (i * 15), 0, 0.75 + (sin((getFrames() / 30.0) + (i * 15)) * 0.25), 0.75 + (sin((getFrames() / 30.0) + (i * 15)) * 0.25))
            }
            break
        case "honey":
            setDrawColor(0xf8f80060)
            drawRect(x - shape.w - floor(camx), y - shape.h - camy, (shape.w * 2) - 1, (shape.h * 2) + 2, true)
            for(local i = 0; i < shape.w / 8; i++) {
                xRand = x - shape.w - floor(camx) + (i * 16);
                drawSpriteEx(sprHoneySurface, (getFrames() / 32) + i, xRand, y - shape.h - camy - 4, 0, 0, 1, 1, 0.5)
            }
            break
        case "swamp":
            setDrawColor(0x68301860)
            drawRect(x - shape.w - floor(camx), y - shape.h - camy, (shape.w * 2) - 1, (shape.h * 2) + 2, true)
            for(local i = 0; i < shape.w / 8; i++) {
                xRand = x - shape.w - floor(camx) + (i * 16);
                drawSpriteEx(sprSwampSurface, (getFrames() / 32) + i, xRand, y - shape.h - camy - 4, 0, 0, 1, 1, 0.5)
            }
            break
        default:
            setDrawColor(0x2020a060)
            drawRect(x - shape.w - floor(camx), y - shape.h - camy, (shape.w * 2) - 1, (shape.h * 2) + 2, true)
            for(local i = 0; i < shape.w / 8; i++) {
                xRand = x - shape.w - floor(camx) + (i * 16);
                drawSpriteEx(sprWaterSurface, (getFrames() / 16) + i, xRand, y - shape.h - camy - 4, 0, 0, 1, 1, 0.5)
            }
            break
    }
}

	function _typeof() { return "Water" }
}
