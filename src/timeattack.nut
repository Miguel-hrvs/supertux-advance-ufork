::gvNextLevel <- ""
::gvTimeAttack <- false
::gvTAStart <- "aurora-learn"
::gvTAStep <- 0
::gvTACourse <- 0

::tileSearchDirSet <- {} // New hash set for directories

::addTimeAttackWorld <- function(
	displayName, //Name to show in the menu
	list, //List of levels in order they're to be played
	folder //The contrib/ folder of your world
	) {
	if(displayName in gvLangObj["level"])
		displayName = gvLangObj["level"][displayName]
	local tempBack = meTimeAttackWorld.pop() //To move back to the end
	local newSlot = {
		name = function() { return displayName }
		func = function() {
			local gfxPath = "contrib/" + folder + "/gfx" // Precomputed path
			local scriptPath = "contrib/" + folder + "/script.nut" // Precomputed path

			game.path = "contrib/" + folder + "/"
			gvTACourse = list
			menu = meDifficulty

			if(!tileSearchDirSet.rawin(gfxPath)) {
				tileSearchDir.push(gfxPath)
				tileSearchDirSet[gfxPath] <- true // Add to set
			}

			if(fileExists(scriptPath) && !contribDidRun.rawin(folder)) {
				donut(scriptPath)
				contribDidRun[folder] <- true
			}
		}
	}

	meTimeAttackWorld.push(newSlot)
	meTimeAttackWorld.push(tempBack)
}
